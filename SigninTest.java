package wfudev.Testing;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class SigninTest {
    
    
    WebDriver driver;
    String act;
    
 @Test (dataProvider="getData")

 public void Login(String un,String pwd,String exp) throws InterruptedException
 {
      driver.findElement(By.id("email")).sendKeys(un);
      driver.findElement(By.id("password")).sendKeys(pwd);
      
     // driver.findElement(By.xpath("/html/body/app-root/app-login/div[2]/form/div[2]/button/span")).click();  
      driver.findElement(By.cssSelector(".ui-button-text.ui-clickable")).click();
      Thread.sleep(2000);
     act=driver.findElement(By.xpath("/html/body/app-root/app-login/div[2]/form/div[3]")).getText();
     
     // act=driver.findElement(By.xpath("/html/body/app-root/app-login/div[2]/form/div[2]")).getText();
      Assert.assertEquals(act, exp);
 }

@DataProvider
public Object[][] getData()
{
    
    Object[][] data=new Object [3][3];
    
    data[0][0]="agent2@gmail.co";
    data[0][1]="12345678";
    data[0][2]="Please check the information you entered.";
    
    
    data[1][0]="agent1@gmail.co";
    data[1][1]="12345678";
    data[1][2]="Please check the information you entered.";
    
    data[2][0]="agent1@gmail.co";
    data[2][1]="12345678900";
    data[2][2]="Please check the information you entered.";
    
    return data;
}

@BeforeTest()
public void beforeTest()
{
    System.setProperty("webdriver.chrome.driver", "C:\\Users\\RideSoft\\Downloads\\chromedriver.exe");
    driver=new ChromeDriver();
    driver.get("https://wfudev.aumtech.com/login");
    
}

@AfterMethod()
public void formclear()
{
    driver.findElement(By.id("email")).clear();
    driver.findElement(By.id("password")).clear();
}
}
